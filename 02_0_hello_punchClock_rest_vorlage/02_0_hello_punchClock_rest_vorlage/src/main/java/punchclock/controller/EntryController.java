package punchclock.controller;



import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.core.Response;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


import punchclock.domain.Entry;
import punchclock.service.EntryService;

// https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/bind/annotation/ResponseStatus.html
// https://www.baeldung.com/spring-response-status

@RestController
@RequestMapping("/entries")
public class EntryController {
    private EntryService entryService;

    public EntryController(EntryService entryService) {
        this.entryService = entryService;
    }
    
    
    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<Entry> getAllEntries() {
        return entryService.findAllEntries();
    }
    
    @GetMapping("/allresponses")
//    @Produces({ "application/json" })
    public Response getAllEntriesResponse() {
		return Response.status(200)
				.entity(entryService.findAllEntries())
				.build();
	}
    
   // ... 
    
    //.. 
    
    
    

}
